import { SHOW_MODAL } from "@/store/modules/modal/types";

class ValidationError extends Error{
    constructor(messageError, store){
        super(messageError);

        store.commit('modal/' + SHOW_MODAL, { type: "warning", message: messageError, show: true}, { root: true })
    }

}

export default ValidationError;
