import {createPlugin} from './NetworkPlugin';

export default (app) => {
    app.use(createPlugin({
        baseUrl: process.env.VUE_API_URL,
        keyStore: process.env.KEYSTORE
    }));
}