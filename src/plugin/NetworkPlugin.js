import axios from 'axios';
import errorHandler from '@/assets/data/errorHandler';
import ValidationError from './ValidationError';
import {LOADING} from "../store/modules/modal/types";

const httpClient = axios.create();
let context = null;

function Plugin(options) {
    console.log('plugin instance...');
    console.log(options);

    httpClient.defaults.baseURL = options.baseUrl;

    httpClient.interceptors.request.use(function(config) {
        if (options.keyStore) {
            let accessToken = localStorage.getItem(options.keyStore)
            if (accessToken) {
                accessToken = JSON.parse(accessToken);
                if (accessToken.token && accessToken.token.trim() !== "")
                    config.headers.Authorization = `Bearer ${accessToken.token}`;
            }
        }
        return config
    });

    httpClient.interceptors.response.use(function(response) {
        return response;
    }, (error) => {
        let errorResponse = (error.response) ?? error;
        let errorStatus = '';
        let endpoint = '';
        if (errorResponse && errorResponse.status) {
            errorStatus = errorResponse.status;
        }
        if (errorResponse && errorResponse.config && errorResponse.config.url) {
            endpoint = errorResponse.config.url;
        }

        let message = errorHandler["default_500"];
        if (errorStatus >= 400 && errorStatus < 500) {
            let endpointJson = errorHandler[endpoint];
            if (endpointJson) {
                let method = (errorResponse.config && errorResponse.config.method) ?? '';
                let methodJson = endpointJson[method];
                if (methodJson) {
                    let messageError = methodJson[errorStatus] ?? '';
                    if (messageError) {
                        message = messageError;
                    }
                }
            }

        }
        throw new ValidationError(message, context);
    });
}

const executeRequest = (url, config, method) => {
    const reqMethod = method.toLowerCase();
    let promise;
    switch (reqMethod) {
        case "get":
            promise = httpClient.get(url, config);
            break;
        case "post":
            promise = httpClient.post(url, config);
            break;
        case "put":
            promise = httpClient.put(url, config);
            break;
        case "delete":
            promise = httpClient.delete(url, config);
            break;
    }
    return promise;
};

Plugin.prototype.executeRequest = (con, url, config, method = "get") => {
    context = con;
    context.commit('modal/'+LOADING, true, { root: true });
    const reqPromise = executeRequest(url, config, method);
    return new Promise((resolve, reject) => {
        reqPromise
            .then(response => {
                context.commit('modal/' + LOADING, false, { root: true });
                resolve(response);
            })
            .catch(error => {
                context.commit('modal/' + LOADING, false, { root: true });
                reject(error);
            });
    });
};

Plugin.prototype.install = function (app) {
    app.plugin = this;

    app.config.globalProperties.$plugin = this;
};

export function createPlugin(data) {
    return new Plugin(data);
}
