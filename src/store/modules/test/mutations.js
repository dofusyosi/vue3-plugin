import {
    TEST
} from './types';

export const state = {
    test: null
};

export const mutations = {
    [TEST](state, payload) {
        state.test = payload.test;
    }
};