import {initInstance} from "../instance";

import {
    TEST
} from "./types";


export const getTest = (context) => {
    console.log('TEST...');
    initInstance.instance.$plugin.executeRequest(context, "users?delay=1", null, "get")
            .then(response => {
            context.commit(TEST, { meta: "SUCCESS", test: response.data?.data });
        })
        .catch(() => {
            context.commit(TEST, { meta: "ERROR" });
        });
};