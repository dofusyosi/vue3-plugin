import { state, mutations } from './mutations';
import * as actions from './actions';

export default {
    state,
    actions,
    mutations,
    namespaced: true
};
