import {
    LOADING
} from "./types";



export const setLoading = (context, data) => {
    context.commit(LOADING, data);
};