import {
    CLOSE_MODAL,
    SHOW_MODAL,
    LOADING,
} from './types';

export const state = {
    type: 'success',
    message: '',
    show: false,
    loading: false
};

export const mutations = {
    [SHOW_MODAL](state, payload) {
        state.type     = payload.type;
        state.message  = payload.message;
        state.show     = true;
    },
    [CLOSE_MODAL](state) {
        state.show = false;
    },
    [LOADING](state, payload) {
        state.loading = payload;
    }
};