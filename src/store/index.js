import { createStore } from 'vuex'
import modules from "@/store/modules";

export default createStore({
  strict: true,
  modules,
  state: {
  },
  getters: {
  },
  mutations: {
  },
  actions: {
  }
})
