import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import {initInstance} from "@/store/modules/instance";

/* import the fontawesome core */
import { library } from '@fortawesome/fontawesome-svg-core'

/* import font awesome icon component */
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

import plugins from './plugin/config';

/* import specific icons */
import {
    faSpinner
} from '@fortawesome/free-solid-svg-icons'

/* add icons to the library */
library.add(
    faSpinner
)

const app = createApp(App)
    .component('font-awesome-icon', FontAwesomeIcon)
    .use(plugins)
    .use(store)
    .use(router)
    .mount('#app')

initInstance.instance = app;